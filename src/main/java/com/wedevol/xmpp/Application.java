package com.wedevol.xmpp;

import com.wedevol.xmpp.server.CcsClient;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.XMPPException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@SpringBootApplication
public class Application {

    private static final Logger logger = Logger.getLogger(Application.class.getName());

    static final String fcmProjectSenderId = "53733553647";
    static final String key = "AIzaSyBfItVhZDSCtG14YrTL5OWLyGivfjFYxG0";

    static Map<String, CcsClient> ccsClients = new HashMap<>();
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
            SASLAuthentication.supportSASLMechanism("PLAIN", 0);
            ccsClients.put("848107427964", CcsClient.prepareClient("848107427964", "AIzaSyCL05tt3P_nVImG_r5SBLQibDITwhavqqg", false));
            ccsClients.keySet().forEach( senderId -> {
                try {
                    ccsClients.get(senderId).connect();
                } catch (XMPPException e) {
                    logger.log(Level.SEVERE, "Error trying to connect.", e);
                }
            });

        /*CcsClient ccsClient = CcsClient.prepareClient(fcmProjectSenderId, fcmServerKey, false);
        try {
            SASLAuthentication.supportSASLMechanism("PLAIN", 0);
            ccsClient.connect();
        }*/
    }

    public static CcsClient getConnection(String senderId) {
        return ccsClients.get(senderId);
    }


}
