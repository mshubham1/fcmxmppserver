package com.wedevol.xmpp.api;

public interface NotificationService {

    void sendNotification(String senderId, String regId, String notifcationData);

}
