package com.wedevol.xmpp.api.impl;

import com.google.gson.Gson;
import com.wedevol.xmpp.Application;
import com.wedevol.xmpp.api.NotificationService;
import com.wedevol.xmpp.bean.CcsOutMessage;
import com.wedevol.xmpp.server.CcsClient;
import com.wedevol.xmpp.server.MessageHelper;
import com.wedevol.xmpp.util.Util;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.XMPPException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class NotificationServiceImpl implements NotificationService {

    private static final Logger logger = Logger.getLogger(NotificationServiceImpl.class.getName());

    @Override
    public void sendNotification(String senderId, String regId, String notificationData) {
        Map<String, String> data = new Gson().fromJson(notificationData, HashMap.class);
        logger.log(Level.INFO, data.toString());
        System.out.println(data);
        String messageId = Util.getUniqueMessageId();
        CcsOutMessage payload = new CcsOutMessage(regId, messageId, data);
        String jsonRequest = MessageHelper.createJsonOutMessage(payload);
        CcsClient client = Application.getConnection(senderId);
        client.send(jsonRequest);
    }
}
