package com.wedevol.xmpp.notification.input;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class NotificationInput {

    private String senderId;
    private String regId;
    private String notificationData;

}
