package com.wedevol.xmpp;

import com.wedevol.xmpp.api.NotificationService;
import com.wedevol.xmpp.notification.input.NotificationInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NotificationController {

    @Autowired
    private NotificationService notificationService;

    @RequestMapping("/")
    @ResponseBody
    public String welcome() {
        return "Welcome to Notification Service";
    }

    @RequestMapping(value = "/notification/send", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<String> sendNotification(NotificationInput input) {
        notificationService.sendNotification(input.getSenderId(), input.getRegId(), input.getNotificationData());
        return ResponseEntity.ok().build();
    }

}

